# packages
jwt    = require 'jwt-simple'
moment = require 'moment'

module.exports = ( secret, tokenTimeout ) -> ( req, res, next ) ->

  return next() if req.method is 'OPTIONS'

  return next() if req.isPublic

  token = req.headers?.authorization?.replace /^Bearer\s+/, ''

  return res.sendStatus 401 if not token

  try
    user = jwt.decode token, secret
  catch e
    return res.sendStatus 401

  req.user = user

  # keep token alive
  if tokenTimeout
    user.exp  = moment().add( tokenTimeout, 'minutes' ).unix()

  req.token = jwt.encode user, secret

  next()

